(ns reframe-handler.runner
    (:require [doo.runner :refer-macros [doo-tests]]
              [reframe-handler.core-test]))

(doo-tests 'reframe-handler.core-test)
