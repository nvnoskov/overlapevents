(ns reframe-handler.subs
  (:require
   [re-frame.core :as rf]))

(rf/reg-sub
 ::name
 (fn [db]
   (:name db)))

(rf/reg-sub
 ::active-panel
 (fn [db _]
   (:active-panel db)))
;
(defn sorted-events
  [db _]
  (:events db))

(defn overlap-events
  [db _]
  (:overevents db))


(rf/reg-sub :sorted-events sorted-events)    ;; usage: (subscribe [:sorted-events])


(rf/reg-sub :overlapevents overlap-events)    ;; usage: (subscribe [:sorted-events])


(rf/reg-sub
  :events        ;; usage:   (subscribe [:events])

  (fn [query-v _]
    (rf/subscribe [:sorted-events]))    ;; returns a single input signal

  (fn [sorted-events query-v _]
    (vals sorted-events)))


(rf/reg-sub
 ::all-complete?
 :<- [:events]
 (fn [events _]
   (every? :done events)))

(rf/reg-sub
 ::completed-count
 :<- [:events]
 (fn [events _]
   (count (filter :done events))))

(rf/reg-sub
 ::footer-counts
 :<- [:events]
 :<- [:completed-count]
 (fn [[events completed] _]
   [(- (count events) completed) completed]))
