(ns reframe-handler.events
  (:require

    [day8.re-frame.http-fx]
    [ajax.core :refer [json-request-format json-response-format]]
    [day8.re-frame.tracing :refer-macros [fn-traced defn-traced]]
    [reframe-handler.db :refer [default-db events->local-store]]
    [re-frame.core :refer [reg-event-db reg-event-fx inject-cofx path after]]
    [cljs.spec.alpha :as s]
    [cljs-time.coerce :refer [to-long]]
    [reframe-handler.overlapping :as overlap]
    ))




(defn add-epoch [dateStart dateEnd coll]
  "Takes date identifier and adds :epoch (cljs-time.coerce/to-long) timestamp to coll"
  (map (fn [item] (assoc item :startEpoch (to-long (dateStart item)) :endEpoch (to-long (dateEnd item))
                              )) coll))

(defn index-by [key coll]
  "Transform a coll to a map with a given key as a lookup value"
  (into {} (map (juxt key identity) (add-epoch :startDate :endDate coll))))

(defn check-and-throw
  "Throws an exception if `db` doesn't match the Spec `a-spec`."
  [a-spec db]
  (when-not (s/valid? a-spec db)
    (throw (ex-info (str "spec check failed: " (s/explain-str a-spec db)) {}))))

;; now we create an interceptor using `after`
(def check-spec-interceptor (after (partial check-and-throw :reframe-handler.db/db)))


(reg-event-fx                                               ;; part of the re-frame API
  :initialise-db                                            ;; event id being handled

  ;; the interceptor chain (a vector of 2 interceptors in this case)
  [(inject-cofx :local-store-events)                        ;; gets events from localstore, and puts value into coeffects arg
   check-spec-interceptor]                                  ;; after event handler runs, check app-db for correctness. Does it still match Spec?

  ;; the event handler (function) being registered
  (fn [{:keys [_ local-store-events]} _]                   ;; take 2 values from coeffects. Ignore event vector itself.
    {:db (assoc default-db :events local-store-events)}))


(def ->local-store (after events->local-store))

(def event-interceptors [check-spec-interceptor             ;; ensure the spec is still valid  (after)
                         (path :events)                     ;; the 1st param given to handler will be the value from this path within db
                         ->local-store])                    ;; write events to localstore  (after)

(defn allocate-next-id
  "Returns the next event id.
  Assumes events are sorted.
  Returns one more than the current largest id."
  [events]
  ((fnil inc 0) (last (keys events))))

(reg-event-db
  ::initialize-db
  (fn-traced [_ _]
             default-db))

(reg-event-db
  ::set-active-panel
  (fn-traced [db [_ active-panel]]
             (assoc db :active-panel active-panel)))



;; -- GET Articles @ /api/events --------------------------------------------
;;
(reg-event-fx                                               ;; usage (dispatch [:get-events {:limit 10 :tag "tag-name" ...}])
  :get-events                                               ;; triggered every time user request events with differetn params
  (fn-traced [{:keys [db]} [_ params]]                      ;; params = {:limit 10 :tag "tag-name" ...}
             {:http-xhrio {:method          :get
                           :uri             (str "/data/" params ".json") ;; evaluates to "api/events/"
                           :params          params          ;; include params in the request
                           ;:headers         (auth-header db)                         ;; get and pass user token obtained during login
                           :response-format (json-response-format {:keywords? true}) ;; json response and all keys to keywords
                           :on-success      [:get-events-success] ;; trigger get-events-success event
                           :on-failure      [:api-request-error :get-events]} ;; trigger api-request-error with :get-events
              :db         (-> db
                              (assoc-in [:loading :events] true)
                              (assoc-in [:filter :offset] (:offset params)) ;; base on paassed params set a filter
                              (assoc-in [:filter :tag] (:tag params)) ;; so that we can easily show and hide
                              (assoc-in [:filter :author] (:author params)) ;; appropriate ui components
                              (assoc-in [:filter :favorites] (:favorited params))
                              (assoc-in [:filter :feed] false))})) ;; we need to disable filter by feed every time since it's not supported query param

(reg-event-db
  :get-events-success
  (fn-traced [db [type events]]
             (-> db
                 (assoc-in [:loading :events] false)        ;; turn off loading flag for this event
                 (assoc-in [:active-panel] :about-panel)    ;; turn off loading flag for this event
                 (assoc :events (merge (:events db) (index-by :url events)))
                 (assoc :newevents (merge (:events db) (index-by :url events)))
                 ))) ;; and events, which we index-by slug

(reg-event-db
  :find-overlaps
  (fn-traced [db [_ temp]]
             (-> db
                 ;(println (overlap/find-overlap (:events db)))
                 (assoc-in [:active-panel] :about-panel)    ;; TODO I don't know why I must rewrite all state at every step.
                 (assoc :overevents (overlap/find-overlap (:events db)))
                 (assoc :events (:events db))
                 )
             ))

;; -- Request Handlers -----------------------------------------------------------
;;
(reg-event-db
  :api-request-error                                        ;; triggered when we get request-error from the server
  (fn-traced [db [_ request-type response]]                 ;; destructure to obtain request-type and response
             (-> db                                         ;; when we complete a request we need to clean so that our ui is nice and tidy
                 (assoc-in [:errors request-type] (get-in response [:response :errors]))
                 (assoc-in [:loading request-type] false))))