(ns reframe-handler.views
  (:require
    [reagent.core :as reagent]
    [re-frame.core :as rf]
    [re-com.core :as re-com]
    [reframe-handler.subs :as subs]
    [clojure.string :as str]
    ))


;; home

(defn home-title []
  (let [name (rf/subscribe [::subs/name])]
    [re-com/title
     :label "the Home Page."
     :level :level1]))

(defn link-to-about-page []
  [re-com/hyperlink-href
   :label "go to Event Page"
   :href "#/about"])

(defn home-panel []
  [re-com/v-box
   :gap "1em"
   :children [[home-title]
              [link-to-about-page]
              ]])


;; about

(defn about-title []
  [re-com/title
   :label "Overlapping events."
   :level :level1])

(defn link-to-home-page []
  [re-com/hyperlink-href
   :label "go to Home Page"
   :href "#/"])


(defn event-input [{:keys [title on-save on-stop]}]
  (let [val (reagent/atom title)
        stop #(do (reset! val "")
                  (when on-stop (on-stop)))
        save #(let [v (-> @val str str/trim)]
                (on-save v)
                (stop))]
    (fn [props]
      [:input (merge (dissoc props :on-save :on-stop :title)
                     {:type        "text"
                      :value       @val
                      :auto-focus  true
                      :on-blur     save
                      :on-change   #(reset! val (-> % .-target .-value))
                      :on-key-down #(case (.-which %)
                                      13 (save)
                                      27 (stop)
                                      nil)})])))


(defn event-item
  []
    (fn [{ :keys [ name url startDate endDate ]}]
      [:a.list-group-item
       [:h4.list-group-item-heading name]
       [:p.list-group-item-text
        [:i url]]
        [:p.list-group-item-text
        (str startDate " - " endDate )
        ]]))


(defn event-table-item
  []
    (fn [{ :keys [ name startDate endDate ]}]
      [:a.list-group-item
       [:h4.list-group-item-heading name]
       [:p.list-group-item-text
        (str startDate " - " endDate )
        ]]))


(defn event-list
  []
  (let [events @(rf/subscribe [:events])]
    [:section#main
     [:div.list-group
      (for [event events]
        ^{:key (:url event)} [event-item event])]
     ]))

(defn overlap-event-list
  []
  (let [events @(rf/subscribe [:overlapevents])]
    [:section#main
     [:div.list-group
      [:table.table.table-striped
       [ :thead
       [:tr
        [:th "Event"]
        [:th "Overlap with"]
        ]]
       [:tbody
      (for [[index event] (map-indexed vector events)]
        ^{:key index}

           [:tr
            [:td [event-table-item (first event)]]
            [:td [event-table-item (second event)]]
            ]


        )]]
      ]
     ]))




(defn events-list
  []
  [:div.container
   [:footer#info
    [:div.btn-group
    [:button.btn.btn-info
     {
      :on-click #(rf/dispatch [:get-events "clojure"])
      } "Clojure Conferences"]
    [:button.btn.btn-info
     {
      :on-click #(rf/dispatch [:get-events "javascript"])
      } "JavaScript Conferences"]
    [:button.btn.btn-info
     {
      :on-click #(rf/dispatch [:get-events "devops"])
      } "DevOps Conferences"]
    [:button.btn.btn-info
     {
      :on-click #(rf/dispatch [:get-events "elixir"])
      } "Elixir Conferences"]
    [:button.btn.btn-warning
     {
      :on-click #(rf/dispatch [:find-overlaps])
      } "Find Overlaps"]
    ]
    ]

   [:section#eventapp
    [:div.row
     [:div.col-md-6 [event-list]]
     [:div.col-md-6 [overlap-event-list]]
     ]


    ]])

(defn about-panel []
  [re-com/v-box
   :gap "1em"
   :children [[about-title]
              [link-to-home-page]
              [events-list]]])


;; main

(defn- panels [panel-name]
  (case panel-name
    :home-panel [home-panel]
    :about-panel [about-panel]
    [:div]))

(defn show-panel [panel-name]
  [panels panel-name])

(defn main-panel []
  (let [active-panel (rf/subscribe [::subs/active-panel])]
    [re-com/v-box
     :height "100%"
     :children [[panels @active-panel]]]))
