(ns reframe-handler.overlapping)


(defn is-overlapping? [a b] (< (:startEpoch b) (:endEpoch a)))

(defn overlap-for-event [e others]
  (->> others
       (filter (partial is-overlapping? e))
       (map (fn [e2] [e, e2]))))

(defn overlapping-events-recurse [events]
  (if (empty? events)
      []
      (concat (overlap-for-event (first events) (rest events))
              (overlapping-events-recurse (rest events)))))


(defn find-overlap
  [coll]
  (overlapping-events-recurse  (sort-by :startEpoch (vec (vals coll)))))


;; Code BEAM SF Clojur
;; :startDate 2019-02-27, :endEpoch 1551484800000, :endDate 2019-03-02, :startEpoch 155122560000

;;Code BEAM SF
;; :startDate 2019-02-28, :endEpoch 1551398400000, :endDate 2019-03-01, :startEpoch 1551312000000