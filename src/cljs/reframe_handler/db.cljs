(ns reframe-handler.db
  (:require [cljs.reader]
            [cljs.spec.alpha :as s]
            [cljs-time.core :as t]
            [re-frame.core :as rf]))


(s/def ::id int?)
(s/def ::name string?)
(s/def ::url string?)
;; (s/def ::startDate t/date-time)
;; (s/def ::endDate t/date-time)
(s/def ::startDate string?)
(s/def ::endDate string?)
(s/def ::city string?)
(s/def ::country string?)

(s/def ::event (s/keys :req-un [::id ::name ::url ::startDate ::endDate ::city ::country]))
(s/def ::events (s/and                                      ;; should use the :kind kw to s/map-of (not supported yet)
                  (s/map-of ::id ::event)                   ;; in this map, each event is keyed by its :id
                  #(instance? PersistentTreeMap %)          ;; is a sorted-map (not just a map)
                  ))
(s/def ::showing                                            ;; what events are shown to the user?
  #{:all                                                    ;; all events are shown
    :active                                                 ;; only events whose :done is false
    :done                                                   ;; only events whose :done is true
    })
(s/def ::db (s/keys :req-un [::events ::showing]))

;; -- Default app-db Value  ---------------------------------------------------
;;
;; When the application first starts, this will be the value put in app-db
;; Unless, of course, there are events in the LocalStore (see further below)
;; Look in:
;;   1.  `core.cljs` for  "(dispatch-sync [:initialise-db])"
;;   2.  `events.cljs` for the registration of :initialise-db handler
;;

(def default-db                                             ;; what gets put into app-db by default.
  {:events (sorted-map)                                     ; '{:id 1 :name text :city "Almaty" :url "https://test.test" :country "Belarus" :startDate "2019-02-22" :endDate "2019-02-22"}
   :languages '("javascript", "clojure")
   :showing
           :all})                                           ;; show all events


;; -- Local Storage  ----------------------------------------------------------
;;
;; Part of the eventmvc challenge is to store events in LocalStorage, and
;; on app startup, reload the events from when the program was last run.
;; But the challenge stipulates to NOT load the setting for the "showing"
;; filter. Just the events.
;;

(def ls-key "events-reframe")                               ;; localstore key

(defn events->local-store
  "Puts events into localStorage"
  [events]
  (.setItem js/localStorage ls-key (str events)))           ;; sorted-map written as an EDN map

(rf/reg-cofx
  :local-store-events
  (fn [cofx _]
    ;; put the localstore events into the coeffect under :local-store-events
    (assoc cofx :local-store-events
                ;; read in events from localstore, and process into a sorted map
                (into (sorted-map)
                      (some->> (.getItem js/localStorage ls-key)
                               (cljs.reader/read-string)    ;; EDN map -> map
                               )))))
