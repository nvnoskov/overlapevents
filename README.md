## Home project "Double Booked"

When maintaining a calendar of events, it is important to know if an event overlaps with another event.

Given a sequence of events, each having a start and end time, write a program that will return the sequence of all pairs of overlapping events.

#### Part I - Algorytm

To find an overlapping event we should:

1) Sort all intervals in increasing order of start time.
2) In the sorted array, if the start time of an interval is less than the end of the previous interval, then there is an overlap.

Otherwise, we can sort by end time and then compare it to start time. It will work too.

#### Part II - Usage

Just to figure out what is Clojure and ClojureScript development is,
I decided to solve this task in Clojure.

That was an interesting and hard experience, functional programming is a new paradigm for me, 
so please don't judge me harshly if I did smth obliviously stupid :)

My personal goal was to achieve an understanding of Clojure development, especially ClojureScript.
How to make web, dom, request, web-socket, etc. 

I found [re-frame](https://github.com/Day8/re-frame) really interesting technics to work with state and data.
(really similar with redux)
I used `reframe_handler` to handle routes, then I used `todomvc` solution as an example of data manipulating and merged with `real-world` example with fetching data solutions  

And I get Conference data from [confs.tech](https://github.com/tech-conferences/conference-data/) just to implement some practical usage of the task.

Core solution for task located in `overlapping.cljs` and triggered by event handler from application to defining a list of overlapping events and show them on an interface

![Overlap](/resources/public/img/overlap.png)     

